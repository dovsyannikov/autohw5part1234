package com.company.classwork8;

public class MadeInUSA extends MadeInEurope {

    public MadeInUSA(String color, String form) {
        super(color, form);
        System.out.println("Made in USA class");
    }

    public MadeInUSA(double engineV, String id, String idEurope) {
        super(engineV, id, idEurope);
    }
}
