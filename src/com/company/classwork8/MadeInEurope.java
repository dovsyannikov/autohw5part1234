package com.company.classwork8;

public class MadeInEurope extends Car {
    private String idEurope;

    public MadeInEurope(String idEurope) {
        this.idEurope = idEurope;
    }

    public MadeInEurope() {
        System.out.println("Made in Europe class");
    }

    public MadeInEurope(String color, String form) {
        super(color, form);
        System.out.println("made in europe class");
    }

    public MadeInEurope(double engineV, String id, String idEurope) {
        super(engineV, id);
        this.idEurope = idEurope;
    }

    public MadeInEurope(String color, String form, double engineV, double fuelConsumption, String id, double fuelInTank) {
        super(color, form, engineV, fuelConsumption, id, fuelInTank);
    }

    @Override
    void fillUpTheTank(int fuelVolume) {
        super.fillUpTheTank(fuelVolume);
        System.out.println("call from the gettersSetters.MadeInEurope class");
    }

    void payTheTaxes(){
        System.out.println("pay a taxes for your Europe car");
    }
}
