package com.company.classwork8;

public class MadeInAsia extends Car {
    public MadeInAsia(String color, String form) {
        super(color, form);
    }

    public MadeInAsia() {
        super();
        System.out.println("Asian car");
    }
}
