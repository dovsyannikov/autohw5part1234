package com.company.homework6;

public class Task4 {
    public static void task4() {
        /*4) Четвертое задание

        Создайте массив размер которого = 10. Заполните его любимы числами.
        Скопируйте все значения этого массива в другой массив в обратном порядке (10, 9, 8 …)
        */

        int[] array1 = {0, 1, 2, 3, 4, 5, 6, 7, 8, 9};
        int[] array2 = new int[10];
        int j = 0;

        for (int i = 9; i >= 0; i--) {
            array2[j++] = array1[i];
            if (i == 0) {
                for (int k = 0; k < 10; k++) {
                    System.out.print(array2[k] + " ");
                }
            }
        }
    }
}
