package com.company.homework7.oopexample;

public class SecondExample {
    private int age;
    private int weight;
    private int height;

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public int getWeight() {
        return weight;
    }

    public void setWeight(int weight) {
        this.weight = weight;
    }

    public int getHeight() {
        return height;
    }

    public void setHeight(int height) {
        this.height = height;
    }

    public SecondExample() {
    }

    public void showAgeValue() {
        System.out.println(getAge());
    }

    public void showWeightValue() {
        System.out.println(getWeight());
    }

    public void showHeightValue() {
        System.out.println(getHeight());
    }


    public SecondExample(int age, int weight) {
        this.age = age;
        this.weight = weight;
    }

    public SecondExample(int age, int weight, int height) {
        this(age, weight);
        this.height = height;
    }
}
