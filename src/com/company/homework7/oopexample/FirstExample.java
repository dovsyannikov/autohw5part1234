package com.company.homework7.oopexample;

public class FirstExample {

//    public static void main(String[] args) {
//        // write your code here
//        Human human = new Human();
//        human.setName("Sobaka");
//        human.print();
//        System.out.println(human.getName());
//    }
}

class Human {
    String name;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    void print() {
        System.out.println(name);
    }
}
