package com.company.homework7.firstexample;

public class Class1 {
    private double numberFirst = 1.0;
    private double numberSecond = 2.0;
    private double numberThird = 3.0;
    private double numberFour = 4.0;

    public double getNumberFirst() {
        return numberFirst;
    }

    public void setNumberFirst(double numberFirst) {
        this.numberFirst = numberFirst;
    }

    public double getNumberSecond() {
        return numberSecond;
    }

    public void setNumberSecond(double numberSecond) {
        this.numberSecond = numberSecond;
    }

    public double getNumberThird() {
        return numberThird;
    }

    public void setNumberThird(double numberThird) {
        this.numberThird = numberThird;
    }

    public double getNumberFour() {
        return numberFour;
    }

    public void setNumberFour(double numberFour) {
        this.numberFour = numberFour;
    }

    public void showNumberFirst() {
        System.out.println(numberFirst);
    }

    public void showNumberSecond() {
        System.out.println(numberSecond);
    }

    public void showNumberThird() {
        System.out.println(numberThird);
    }

    public void showNumberFour() {
        System.out.println(numberFour);
    }

}
