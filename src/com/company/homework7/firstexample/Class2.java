package com.company.homework7.firstexample;

import com.company.homework7.firstexample.Class1;

public class Class2 {

    public void showHowToWorksModificators(){
        Class1 class1 = new Class1();

        System.out.println(class1.getNumberFirst());
        System.out.println(class1.getNumberSecond());
        System.out.println(class1.getNumberFour());

        class1.showNumberFirst();
        class1.showNumberSecond();
        class1.showNumberFour();
    }
}
