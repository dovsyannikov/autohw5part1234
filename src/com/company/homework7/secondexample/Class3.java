package com.company.homework7.secondexample;

import com.company.homework7.firstexample.Class1;
import com.company.homework7.firstexample.Class2;

public class Class3 extends Class1 {

    public void showHowToWorksModificators() {
        Class1 class1 = new Class1();
        Class2 class2 = new Class2();

        System.out.println(class1.getNumberFirst());

        class1.showNumberFirst();
    }
}
